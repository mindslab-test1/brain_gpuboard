import re
import sys
from enum import IntEnum
from io import StringIO
from datetime import datetime, timezone, timedelta

import gpustat

KOR_TZ = timezone(timedelta(hours=9))

class Column(IntEnum):
    DEVICE_TYPE = 0
    UTILS = 1
    MEMORY_USAGE = 2
    PROCESSES = 3

def get_gpu_stat():
    gpustat_info = _catch_gpustat()

    misc = gpustat_info[0]  # users, graphic driver version included
    gpus_info = [x for x in gpustat_info[1:] if len(x) != 0]
    gpus_info = [x.split("|") for x in gpus_info]  # simple parser
    gpu_info_batch = [_extract_ouput(single_gpu_info) for single_gpu_info in gpus_info]
    return gpu_info_batch

def _catch_gpustat():
    old_stdout = sys.stdout
    std_out = StringIO()
    sys.stdout = std_out
    gpustat.print_gpustat(no_color=True)
    sys.stdout = old_stdout
    gpustat_info = std_out.getvalue().split("\n")
    return gpustat_info

def _extract_ouput(_single_info):
    gpu_type = _single_info[Column.DEVICE_TYPE].strip()
    temperature, volatile = [util_value.strip() for util_value in _single_info[Column.UTILS].split(',')]
    memory_used, memory_full = re.findall("\d+", _single_info[Column.MEMORY_USAGE])

    regex = re.compile("[0-9a-zA-Z]{1,}\([0-9]{1,}M\)")
    processes = regex.findall(_single_info[Column.PROCESSES])
    return [gpu_type, temperature, volatile, memory_used, memory_full, processes]


def get_now_strftime():
    now_str =  datetime.now().astimezone(KOR_TZ).strftime('%Y-%m-%d %H:%M:%S')
    return now_str

if __name__ == '__main__':
    print(get_gpu_stat())
