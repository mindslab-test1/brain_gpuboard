import gspread
from oauth2client.service_account import ServiceAccountCredentials


def get_worksheet_with_auth(credential_path, scope, sheet_url):
    credentials = ServiceAccountCredentials.from_json_keyfile_name(credential_path, scope)
    gc = gspread.authorize(credentials)

    # Open a sheet from a spreadsheet in one go
    worksheets = gc.open_by_url(sheet_url)
    worksheet = worksheets.get_worksheet(0)
    return worksheet


def num2alpha(val):
    return chr(64 + val)  # val: 1 -> A