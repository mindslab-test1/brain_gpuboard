import argparse
import logging
import time

from util.gpustat import get_gpu_stat, get_now_strftime
from util.spreadsheet import get_worksheet_with_auth, num2alpha

PERIOD = 60 * 5  # seconds
START_COL = 3

MAX_COL_NUM = 10

class GPUReporter:
    def __init__(self, json_file_name, interval):
        scope = [
            'https://spreadsheets.google.com/feeds',
            'https://www.googleapis.com/auth/drive',
        ]

        self.worksheet = get_worksheet_with_auth(json_file_name, scope,
                                                 'https://docs.google.com/spreadsheets/d/1eUfIW7OcKmacGHmCgm5BM6CmIfU6Fb4KPmjNSYPq1_M')

        if interval == 0:
            self.interval = PERIOD
        elif interval >= 60:
            self.interval = interval
        else:
            logging.info(f"Interval should be longer than 60 seconds. "
                         f"{interval} seconds should be adjusted to 60 seconds automatically.")
            self.interval = 60

    def update_worksheet(self):
        try:
            cell = self.worksheet.find(args.server_key)
        except RuntimeError as e:
            raise e
        
        try:
            start_row = cell.row
            gpu_status_list = self._get_gpu_status()
        except:
            raise RuntimeError(f"Server key {args.server_key} may have some problems. Please check it again.")

        try:
            for idx, gpu_info in enumerate(gpu_status_list):
                gpu_status = [get_now_strftime()] + gpu_info[:-1]

                num_status = len(gpu_status)

                # update gpu status
                self.worksheet.update('{:s}{:d}:{:s}{:d}'.format(num2alpha(START_COL), start_row,
                                                                num2alpha(num_status + START_COL), start_row),
                                    [gpu_status])

                # update process list
                processes = gpu_info[-1][:]
                num_processes = min(len(processes), MAX_COL_NUM)
                processes = processes + ["" for _ in range(MAX_COL_NUM - num_processes)]
                processes = processes[:MAX_COL_NUM]
                self.worksheet.update('{:s}{:d}:{:s}{:d}'.format(num2alpha(num_status + START_COL), start_row,
                                                                num2alpha(num_status + START_COL + MAX_COL_NUM), start_row),
                                    [processes])

                start_row += 1
        except RuntimeError as e:
            raise e

    def _get_gpu_status(self):
        return get_gpu_stat()

    def get_interval(self):
        return self.interval


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='server key for gpu')
    parser.add_argument('-s', '--server_key',
                        type=str,
                        required=True,
                        help="unique 8 letters hexa key for server, which can be found in spreadsheet itself.")
    parser.add_argument('-c', '--credential',
                        type=str,
                        default='brain-gpuboard-1612835045189-1095efeb358f.json',
                        help="credential file path for authentication")
    parser.add_argument('-i', '--interval',
                        type=int,
                        default=0,
                        help="interval for logging stats (default: 300 secs)")
    parser.add_argument('--log_level',
                        type=str,
                        default='INFO',
                        help="logging level (default: INFO)")

    args = parser.parse_args()
    reporter = GPUReporter(args.credential, args.interval)

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )

    logger = logging.getLogger("gpu-reporter")
    # file_handler = logging.FileHandler('./logs/log_server.log')
    # logger.addHandler(file_handler)

    try:
        while True:
            try:
                reporter.update_worksheet()
                logger.info(f"Succesfully update GPU status.")
            except RuntimeError as e:
                logger.info(str(e))
            except:
                logger.warn("Error occurs ... which is not RuntimeError!")
            finally:
                time.sleep(reporter.get_interval())
    except KeyboardInterrupt:
        pass