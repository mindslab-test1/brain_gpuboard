FROM docker.maum.ai:443/brain/vision:cu101

COPY ./requirements.txt /app/
WORKDIR /app
RUN pip install --ignore-installed -r requirements.txt

COPY . /app


ENV SERVER_KEY 12345678
ENV CREDENTIAL /credential.json
ENV INTERVAL 0
ENV LOG_LEVEL INFO

ENTRYPOINT python server.py --server_key $SERVER_KEY --credential $CREDENTIAL --interval $INTERVAL --log_level $LOG_LEVEL